import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import vn.com.application.grpc.CarCenter;
import vn.com.application.grpc.CustomerServiceGrpc;

public class App {
    public static void main(String[] args) {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress("localhost", 1234).usePlaintext().build();
        CustomerServiceGrpc.CustomerServiceBlockingStub customerServiceBlockingStub = CustomerServiceGrpc.newBlockingStub(managedChannel);
        CarCenter.CustomerRequest customerRequest = CarCenter.CustomerRequest.newBuilder().setRequestType(CarCenter.RequestType.BUY).setName("Tuan ml").setCarModel("Mercedes").build();
        CarCenter.CustomerRequest customerRequest1 = CarCenter.CustomerRequest.newBuilder().setRequestType(CarCenter.RequestType.SELL).setName("Tuan ml").setCarModel("Mercedes").build();
        customerServiceBlockingStub.createRequest(customerRequest);
        customerServiceBlockingStub.createRequest(customerRequest1);
//        customerServiceBlockingStub.createRequest(customerRequest1);
//        customerServiceBlockingStub.createRequest(customerRequest1);
//        customerServiceBlockingStub.createRequest(customerRequest1);
//        customerServiceBlockingStub.createRequest(customerRequest1);
//        customerServiceBlockingStub.createRequest(customerRequest1);
//        customerServiceBlockingStub.createRequest(customerRequest1);

        // handshake connection 1 time, for next request -> dont do handshake
        // data dont sent key -> just send value
        // java binary stream mapping object
        // + Rest: object -> json(string) -> binary -> sent
        // + rpc: object -> binary(java code) -> sent


        // Unary call

    }
}
